import json
import requests
import sys

def get_faction(piece):
    if (piece['faction_code'] == 'haas-bioroid'):
        return ("\\hb")
    if (piece['faction_code'] == 'jinteki'):
        return ("\\jinteki")
    if (piece['faction_code'] == 'nbn'):
        return ("\\nbn")
    if (piece['faction_code'] == 'weyland-consortium'):
        return ("\\weyland")
    return ("")

def check_etr(piece):
    if ("Subroutine End the run.") in piece['stripped_text']:
        if ("Bioroid") in piece['keywords']:
            return "\\click"
        return "ETR"
    if ("End the run unless the Runner pays") in piece['stripped_text']:
        return "\\cred"
    return ""

ice = []
setlist = open(sys.argv[1])
sets = setlist.read().splitlines()
for line in sets:
    r = requests.get("https://raw.githubusercontent.com/NetrunnerDB/netrunner-cards-json/main/pack/" + line + ".json")
    loads = json.loads(r.text)
    ice += ([x for x in loads if x['type_code'] == 'ice'])

running_cost = 0
while (len(ice) != 0):
    matching_ice = [piece for piece in ice if piece['cost'] == running_cost]
    ice = [piece for piece in ice if piece['cost'] != running_cost]
    barriers = []
    codegates = []
    sentries = []
    for piece in matching_ice:
        if ('Barrier' in piece['keywords']):
            barriers.append(piece)
        if ('Code Gate' in piece['keywords']):
            codegates.append(piece)
        if ('Sentry' in piece['keywords']):
            sentries.append(piece)
    if (len(barriers) != 0 or len(codegates) != 0 or len (sentries) != 0):
        print("\\\\\n\\hline\n" + str(running_cost) + " \\cred", end=" ")
        first_cycle = True
    while (len(barriers) != 0 or len(codegates) != 0 or len (sentries) != 0):
        if (first_cycle == True):
            first_cycle = False
        else:
            print("\\\\")
        print ("& ", end="")
        if (len(barriers) != 0):
            print(get_faction(barriers[0]), end=" & ")
            print(str(barriers[0]['title']), end=" & ")
            print(str(barriers[0]['strength']), end=" & ")
            print(check_etr(barriers[0]), end=" & ")
            barriers.pop(0)
        else:
            print("& & & & ", end="")
        if (len(codegates) != 0):
            print(get_faction(codegates[0]), end=" & ")
            print(str(codegates[0]['title']), end=" & ")
            print(str(codegates[0]['strength']), end=" & ")
            print(check_etr(codegates[0]), end=" & ")
            codegates.pop(0)
        else:
            print("& & & & ", end="")
        if (len(sentries) != 0):
            print(get_faction(sentries[0]), end=" & ")
            print(str(sentries[0]['title']), end=" & ")
            print(str(sentries[0]['strength']), end=" & ")
            print(check_etr(sentries[0]), end="")
            sentries.pop(0)
        else:
            print("& & & ", end="")
    running_cost += 1
