# ice-cheatsheet

This is a script that fetches card data from [the NetrunnerDB database](https://github.com/NetrunnerDB/netrunner-cards-json/), takes all ice from specific packs, and makes a LaTeX document representing a handy cheatsheet, fit for printing on a5 paper. This is set up to give data for the Startup format, and intended primarily for newer players who are approaching a constructed format for the first time and need some help figuring out what's in the meta and where/when to run.

Ice is ordered vertically by rez cost and divided into columns by subtype. Every entry about a piece of ice contains its faction symbol, its name, its strength and whether it has a hard ETR/Bioroid ETR/payable-through ETR.

The latest version of the cheatsheet can be found on the [Releases](TODO) page. You can also compile the document yourself (by executing the `startup-cheatsheet.sh` script), potentially tweaking it to include different sets.

All software is provided under [GPL](https://gitlab.com/transfixed/ice-cheatsheet/-/blob/main/license) except the netrunner symbols font, which is under [MIT license](https://gitlab.com/transfixed/ice-cheatsheet/-/blob/main/font-license).

Original repo for the netrunner font is [here](https://github.com/MWDelaney/Netrunner-Icon-Font/).

Project avatar is the art for Mayfly from System Gateway, credit goes to [NISEI](https://nisei.net) and [Scott Uminga](https://www.scottuminga.com/).

## Prerequisites

To run cheatsheet.py you need to install [Python](https://www.python.org/), to compile the cheatsheet you need [XeLaTeX](http://xetex.sourceforge.net/) or [LuaLaTeX](http://luatex.org/).

## Todo

* Support for banlist (not immediately needed, since there are no banned cards in Startup yet).
* Fixing cards showing up more than once if they are present in more than one selected set.
